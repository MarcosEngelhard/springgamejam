﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager gameInstance;
    private int score;
    [SerializeField] private Text scoreText;
    [SerializeField] private int scoreMultiplier = 1;
    public bool isGameover = false;
    public bool isOpen = false;
    public bool isFlorwerGrown;
    private ButtonsBehaviour buttonsBehaviour;
    // Start is called before the first frame update
    void Start()
    {
        // GameManager's singleton
        gameInstance = this;
        scoreText.text = score.ToString();
        buttonsBehaviour = FindObjectOfType<ButtonsBehaviour>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void UpdateScore(int amountScore)
    {
        score += amountScore * scoreMultiplier;
        scoreText.text = score.ToString();
    }
    public void SetGameOver(bool isGameOver)
    {
        this.isGameover = isGameOver;
        buttonsBehaviour.DisplayRetry();
    }
    public void IncreaseMultiplier()
    {
        scoreMultiplier++;
    }
    public void DecreaseMultiplier()
    {
        scoreMultiplier--;
        if(scoreMultiplier <= 0)
        {
            scoreMultiplier = 1;
        }
    }
}
