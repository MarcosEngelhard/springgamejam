﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonsBehaviour : MonoBehaviour
{
    [SerializeField]private GameObject mainMenuBackground;
    [SerializeField]private GameObject howToPlayBackground;
    [SerializeField]private GameObject creditsBackground;
    [SerializeField] private Button retryButton;
    [SerializeField] private Text scoreText;
    

    private void Start()
    {
        if(mainMenuBackground != null)
        {
            mainMenuBackground.SetActive(true);
            howToPlayBackground.SetActive(false);
            creditsBackground.SetActive(false);
        }
        if(retryButton != null)
        {
            retryButton.gameObject.SetActive(false);
        }
    }
    public void DisplayRetry()
    {
        retryButton.gameObject.SetActive(true);
    }
    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void RetryGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void DisplayCredits()
    {
        mainMenuBackground.SetActive(false);
        howToPlayBackground.SetActive(false);
        creditsBackground.SetActive(true);
    }
    public void DisplayHowToPlay()
    {
        mainMenuBackground.SetActive(false);
        howToPlayBackground.SetActive(true);
        creditsBackground.SetActive(false);
    }
    public void MainMenu()
    {
        mainMenuBackground.SetActive(true);
        howToPlayBackground.SetActive(false);
        creditsBackground.SetActive(false);
    }

}
