﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
    public Transform target;
    [SerializeField] private Vector3 offset = new Vector3(0, 20f, 0);
    [SerializeField] private float desiredSpeed;
    [SerializeField] private float smoothSpeed = 0.125f;
    private GameObject beginningOfTheFlower;
    private Camera cameraMain;
    private float initialOrtographicSize;
    private ControlPlayer controlPlayer;
    private delegate IEnumerator LoopGame();
    private LoopGame loopGame;
    [SerializeField]private float speedsize = 2f;
    public enum State
    {
        NormalGameplay, Show
    }
    public State state;
    private Deploy deploy;

    private void Awake()
    {
        cameraMain = Camera.main;
        initialOrtographicSize = cameraMain.orthographicSize;
        controlPlayer = FindObjectOfType<ControlPlayer>();
        loopGame = AnotherFlower;
        deploy = FindObjectOfType<Deploy>();
    }
    // Start is called before the first frame update
    void Start()
    {    
    }
    // Update is called once per frame
    void LateUpdate()
    {
        switch (state)
        {
            case State.NormalGameplay:
                Vector3 playerPosY = new Vector3(transform.position.x, target.position.y, transform.position.z);
                Vector3 desiredPosition = playerPosY + offset;
                Vector3 smoothedPosition = Vector3.Slerp(transform.position, desiredPosition, smoothSpeed * Time.deltaTime);
                transform.position = smoothedPosition;
                break;
            case State.Show:
                //if(beginningOfTheFlower.GetComponent<SpriteRenderer>().isVisible) // When we see the complete flower o the screen
                //{
                //    if(loopGame != null)
                //    {
                //        StartCoroutine(loopGame());
                //    }
                //}
                //else
                //{
                //    camera.orthographicSize += speedsize * Time.deltaTime;
                //}
                if(loopGame != null) // see some progress after a few seconds than return to another flower
                {
                    StartCoroutine(loopGame?.Invoke());
                }               
                cameraMain.orthographicSize += speedsize * Time.deltaTime;
                break;
        }       
    }
    public IEnumerator AnotherFlower()
    {
        loopGame = null;
        yield return new WaitForSeconds(3f); // Wait some time to reset some things
        controlPlayer.ReturnSmall();
        cameraMain.orthographicSize = initialOrtographicSize; // reset ortographic size
        state = State.NormalGameplay;
        GameManager.gameInstance.isFlorwerGrown = false;
        deploy.ReduceTime();
        StartCoroutine(deploy.PrepareWave());
        loopGame = AnotherFlower;
    }   
}
