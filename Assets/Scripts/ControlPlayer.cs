﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlPlayer : MonoBehaviour
{
    [SerializeField] private float speed = 2f;
    private Vector2 mousePosition;
    [SerializeField] private Transform RLimit;
    [SerializeField] private Transform LLimit;
    private bool isDead;
    [SerializeField]private Transform TopOfTheFlower;
    private GameObject player; // Top of the flower
    private SpriteRenderer playerSpriteRenderer;
    [SerializeField] private Vector2 scaleChange = new Vector3(0,0.1f);
    [SerializeField] private float growSpeed = 0.75f;
    public Vector3 screenBounds;
    private int Ds = 0;
    private int H2Os;
    private int CO2s;
    public int maxDs = 20;
    public int maxH2Os = 1;
    public int maxCO2s = 2;
    [SerializeField] private Text dsText;
    [SerializeField] private Text h2oText;
    [SerializeField] private Text co2Text;   
    private Vector3 initialScale;
    [SerializeField] private GameObject greenery;
    [SerializeField] private GameObject topOfFlower;
    private SpriteRenderer greenerySpriteRenderer;
    [System.Serializable]
    public struct Flowers
    {      
        public string name;
        public GameObject flowerType;
        public Sprite[] flowerSprite;
        public BoxCollider2D[] bc;       
    }
    public Flowers[] flowerSprites;
    private int floweIndex;
    
    private enum FlowerGrown
    {
        FlowerGronInitial, // = 0
        FlowerGrownd, // = 1
        FlowerGrown2, // = 2
        FlowerGrownFinal // = 3
    }
    [SerializeField]private FlowerGrown flowerGrown;
    private CameraBehaviour cameraBehaviour;
    private AudioSource audioSource;
    [SerializeField] private AudioClip caughtSunlight;
    [SerializeField] private AudioClip caughtH2O;
    [SerializeField] private AudioClip caughtCO2;

    public void IsDead(bool isdead)
    {
        this.isDead = isdead;       
    }
    private void Awake()
    {
        greenery = transform.Find("Verdura").gameObject;
        greenerySpriteRenderer = greenery.GetComponent<SpriteRenderer>();
        cameraBehaviour = FindObjectOfType<CameraBehaviour>();
        audioSource = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        initialScale = greenerySpriteRenderer.size;
        flowerGrown = 0;
        ResetColorAndTest();
        ChooseFlower();
    }

    // Update is called once per frame
    void Update()
    {
        if(isDead || GameManager.gameInstance.isFlorwerGrown) // if it's dead player can't move or is showing how flower has grown
            return;
            mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }
    private void FixedUpdate()
    {
        if (isDead || GameManager.gameInstance.isFlorwerGrown) // if it's dead player can't move or is showing how flower has grown
            return;
        Move();
    }
    private void LateUpdate()
    {
        if(player != null)
        {
            player.transform.position = TopOfTheFlower.position;
        }
        
    }
    private void Move()
    {
        greenerySpriteRenderer.size += scaleChange * growSpeed * Time.deltaTime;
        topOfFlower.transform.localPosition = new Vector2(topOfFlower.transform.localPosition.x, greenerySpriteRenderer.size.y);       
        // FOllow mouse in x axis
        Vector2 desiredPosition = new Vector2(mousePosition.x, transform.position.y);
        transform.position = Vector3.MoveTowards(transform.position, desiredPosition, speed * Time.fixedDeltaTime);
        //Limit position
        float xPos = transform.position.x;
        xPos = Mathf.Clamp(transform.position.x, LLimit.position.x, RLimit.position.x);
        transform.position = new Vector3(xPos, transform.position.y, transform.position.z);
    }

    public void HowManySunlightToGrow()
    {

        Ds++;
        audioSource.PlayOneShot(caughtSunlight);
        dsText.text = Ds.ToString() + '/' + maxDs.ToString() + " Sunlight";
        if (Ds == maxDs)
        {
            flowerGrown++;
            dsText.color = Color.green;
            ChangeTopFlowerSprite();
            CheckFinalForm();
        }

    }
    public void HowManyH2OToGrow()
    {

        H2Os++;
        audioSource.PlayOneShot(caughtH2O);
        h2oText.text = H2Os.ToString() + '/' + maxH2Os.ToString() + " H2O";
        if (H2Os == maxH2Os)
        {
            flowerGrown++;
            h2oText.color = Color.green;
            ChangeTopFlowerSprite();
            CheckFinalForm();
        }
    }
    public void HowManyCO2ToGrow()
    {
        CO2s++;
        audioSource.PlayOneShot(caughtCO2);
        co2Text.text = CO2s.ToString() + '/' + maxCO2s.ToString() + " CO2";
        if (CO2s == maxCO2s)
        {
            flowerGrown++;
            co2Text.color = Color.green;
            ChangeTopFlowerSprite();
            CheckFinalForm();
        }
    }
    public void CheckFinalForm() // Check if have everything to go to a finalform
    {
        if (flowerGrown == FlowerGrown.FlowerGrownFinal)
        {
            FindObjectOfType<CameraBehaviour>().state = CameraBehaviour.State.Show;
            GameManager.gameInstance.isFlorwerGrown = true;
        }
    }
    public void ReturnSmall()
    {
        greenerySpriteRenderer.size = initialScale;
        maxDs += maxDs;
        maxH2Os += maxH2Os;
        maxCO2s += maxCO2s;
        flowerGrown = 0;
        ResetColorAndTest();
    }
    private void ResetColorAndTest()
    { // "Update UI" to the max and put the color back to black
        // Reset to all tasks to zero
        Ds = 0;
        H2Os = 0;
        CO2s = 0;
        dsText.text = Ds.ToString() +'/' +  maxDs.ToString() + " Sunlight";
        dsText.color = Color.black;
        h2oText.text = H2Os.ToString() + '/' + maxH2Os.ToString() + " H2O";
        h2oText.color = Color.black;
        co2Text.text = CO2s.ToString() + '/' + maxCO2s.ToString() + " CO2";
        co2Text.color = Color.black;
        
        ChooseFlower();
    }
    private void ChooseFlower()
    {
        // CHoose randomly one flowe type sprite
        floweIndex = Random.Range(0, flowerSprites.Length);
        for (int i = 0; i < flowerSprites.Length; i++)
        {
            if (i == floweIndex)
            { // Enabled the desired flower and go to the top of the flower
                flowerSprites[i].flowerType.SetActive(true);
                player = flowerSprites[i].flowerType;
                player.transform.position = TopOfTheFlower.position;
                playerSpriteRenderer = player.GetComponent<SpriteRenderer>();
                cameraBehaviour.target = player.transform;
            }
            else
            { // Disable the flower which aren't desired
                flowerSprites[i].flowerType.SetActive(false);
            }
        }
        ChangeTopFlowerSprite();
        

    }
    private void ChangeTopFlowerSprite()
    {
        for (int i = 0; i < flowerSprites.Length; i++)
        {
            if (i == floweIndex)
            {
                flowerSprites[i].flowerType.SetActive(true);
                for(int j = 0; j < flowerSprites[i].bc.Length; j++)
                {
                    if(j == (int)flowerGrown)
                    { // enable the desired collider
                        flowerSprites[i].bc[j].enabled = true;
                    }
                    else
                    { // disable the rest of the colliders
                        flowerSprites[i].bc[j].enabled = false;
                    }
                }
                break; // exit loop
            }
        }
        playerSpriteRenderer.sprite = flowerSprites[floweIndex].flowerSprite[(int)flowerGrown + 1];
    }
    
}
