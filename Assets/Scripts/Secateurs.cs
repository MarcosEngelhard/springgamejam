﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Secateurs : MonoBehaviour
{
    [SerializeField] private float speed = 4f;
    private Rigidbody2D rb;
    private Vector2 screenBoundsMax;
    private Vector2 screenBoundsMin;
    private SpriteRenderer spriteRenderer;
    private delegate void KillPlayer(bool isDead);
    private KillPlayer killPlayer;
    private bool isFacingRight = true;
    private void Awake()
    {
        killPlayer += GameManager.gameInstance.SetGameOver;
        killPlayer += FindObjectOfType<ControlPlayer>().IsDead;
        rb = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
    // Start is called before the first frame update
    void Start()
    {
    }
    private void OnEnable()
    {
        rb.velocity = new Vector2(0, -speed);
        screenBoundsMax = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        screenBoundsMin = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, Camera.main.transform.position.z));
        if(transform.position.x < 0 && !isFacingRight)
        {
            Flip();
        }
        else if(transform.position.x > 0 && isFacingRight)
        {
            Flip();
        }
    }
    private void OnDisable()
    {
        spriteRenderer.flipX = false;
        isFacingRight = true;
    }
    private void Flip() // Flip to the right direction
    {
        spriteRenderer.flipX = !spriteRenderer.flipX;
        isFacingRight = !isFacingRight;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            killPlayer?.Invoke(true);
            SecateursPool.intstance.ReturnToPool(this);
        }
    }
    private void OnBecameInvisible()
    {
        if(transform.position.y < screenBoundsMin.y)
        {
            // Can be optimized with pool
            SecateursPool.intstance.ReturnToPool(this);
        }
    }
}
