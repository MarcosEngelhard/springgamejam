﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deploy : MonoBehaviour
{
    [SerializeField] private Transform[] objcts;
    public float respawnTime = 2f;
    private Vector2 screenBound;
    [SerializeField]private Transform LLimit, RLimit;
    [SerializeField] private float startWave = 2f;

    private void Awake()
    {

    }
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(PrepareWave());
        

    }
    public void ReduceTime()
    {
        //Reduce interval between objetcs to spawn ( secateurs and/or orbs)
        respawnTime -= 0.2f;
        if(respawnTime < 1f)
        {
            respawnTime = 1f;
        }
    }

    private void SpawnEnemies()
    {
        int rIndex = Random.Range(0,  objcts.Length);
        switch (rIndex)
        {
            case 0:
                // Enqueu secateur from Secateurs Pool
                if(objcts[rIndex].gameObject.TryGetComponent<SecateursPool>(out SecateursPool secateursPool))
                {
                    var objectDown = secateursPool.Get();
                    objectDown.gameObject.SetActive(true);
                    objectDown.transform.position = new Vector2(Random.Range(LLimit.position.x, RLimit.position.x), screenBound.y);
                }
                
                break;
            default:
                // Enqueu orb from Orb the Pool
                if (objcts[rIndex].gameObject.TryGetComponent<OrbPool>(out OrbPool orbPool))
                {
                    var objectDown = orbPool.Get();
                    objectDown.gameObject.SetActive(true);
                    objectDown.transform.position = new Vector2(Random.Range(LLimit.position.x, RLimit.position.x), screenBound.y);
                }

                break;
            case 2:
                // Enqueu secateur from the H2O Pool
                if (objcts[rIndex].gameObject.TryGetComponent<H2OPool>(out H2OPool h2oPool))
                {
                    var objectDown = h2oPool.Get();
                    objectDown.gameObject.SetActive(true);
                    objectDown.transform.position = new Vector2(Random.Range(LLimit.position.x, RLimit.position.x), screenBound.y);
                }
                break;
            case 3:
                // Enqueu secateur from the Pool
                if (objcts[rIndex].gameObject.TryGetComponent<CO2Pool>(out CO2Pool co2Pool))
                {
                    var objectDown = co2Pool.Get();
                    objectDown.gameObject.SetActive(true);
                    objectDown.transform.position = new Vector2(Random.Range(LLimit.position.x, RLimit.position.x), screenBound.y);
                }
                break;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        screenBound = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
    }
    public IEnumerator PrepareWave()
    {
        yield return new WaitForSeconds(startWave);
        StartCoroutine(Wave());
    }
    private IEnumerator Wave()
    {
        while (!GameManager.gameInstance.isGameover && !GameManager.gameInstance.isFlorwerGrown)
        {
            SpawnEnemies(); // spawn when is nnot game over and flow is not growing either
            yield return new WaitForSeconds(respawnTime);
            
        }
        
    }
}
