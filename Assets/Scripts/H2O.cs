﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class H2O : MonoBehaviour
{
    [SerializeField] private float speed = 4f;
    private Rigidbody2D rb;
    private Vector2 screenBoundsMin;
    private SpriteRenderer spriteRenderer;
    [SerializeField] private int scoreGiven = 15; // how much recieve to score
    private delegate void ToUpdateScore(int num); // defines the delegate
    ToUpdateScore updateScore;
    private delegate void Multiplier();
    Multiplier multiplier;
    private delegate void GoToCount();
    GoToCount toCount;


    private void Awake()
    {
        updateScore = GameManager.gameInstance.UpdateScore; // subscribe function to delegate
        toCount = FindObjectOfType<ControlPlayer>().HowManyH2OToGrow;
    }
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        rb.velocity = new Vector2(0, -speed);
        screenBoundsMin = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, Camera.main.transform.position.z));
    }

    // Update is called once per frame
    void Update()
    {
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            //Hit Player
            updateScore?.Invoke(scoreGiven);
            multiplier = GameManager.gameInstance.IncreaseMultiplier;
            multiplier?.Invoke();
            toCount?.Invoke();
            H2OPool.intstance.ReturnToPool(this);
        }
    }
    private void OnBecameInvisible() // When is not visible on the screen in order to perform
    {
        if (transform.position.y < screenBoundsMin.y)
        {
            multiplier = GameManager.gameInstance.DecreaseMultiplier;
            multiplier?.Invoke();
            // Can be optimized with pool
            H2OPool.intstance.ReturnToPool(this);
        }
    }
}
